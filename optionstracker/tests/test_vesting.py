from unittest import TestCase
from datetime import datetime

from optionstracker.vesting import vested, vesting_profile


class TestVested_annually(TestCase):
    def test_before_grant(self):
        self.assertEqual(
                (0, 200000),
                vested(
                    200000, datetime(2020, 5, 15),
                    datetime(2020, 5, 18), 0.33333, 'y'))

    def test_none_yet(self):
        self.assertEqual(
                (0, 200000),
                vested(
                    200000, datetime(2020, 6, 15),
                    datetime(2020, 5, 18), 0.33333, 'y'))

    def test_one_cycle(self):
        self.assertEqual(
                (66666, 133334),
                vested(
                    200000, datetime(2021, 6, 19),
                    datetime(2020, 5, 18), 0.33333, 'y'))

    def test_all_vested_after_3_years(self):
        self.assertEqual(
                (200000, 0),
                vested(
                    200000, datetime(2023, 6, 19),
                    datetime(2020, 5, 18), 0.33333, 'y'))


class TestVested_monthly(TestCase):
    def test_before_grant(self):
        self.assertEqual(
                (0, 200000),
                vested(
                    200000, datetime(2020, 5, 15),
                    datetime(2020, 5, 18), 0.01))

    def test_none_yet(self):
        self.assertEqual(
                (0, 200000),
                vested(
                    200000, datetime(2020, 6, 15),
                    datetime(2020, 5, 18), 0.01))

    def test_one_cycle(self):
        self.assertEqual(
                (2000, 198000),
                vested(
                    200000, datetime(2020, 6, 19),
                    datetime(2020, 5, 18), 0.01))

    def test_over_a_year(self):
        self.assertEqual(
                (26000, 174000),
                vested(
                    200000, datetime(2021, 6, 19),
                    datetime(2020, 5, 18), 0.01))

    def test_all_vested_after_3_years(self):
        self.assertEqual(
                (200000, 0),
                vested(
                    200000, datetime(2023, 6, 19),
                    datetime(2020, 5, 18), 0.01))


class TestVestingProfile(TestCase):
    def setUp(self):
        self.monthly_profile = vesting_profile(
                    20000, datetime(2020, 5, 5), 0.01)

    def test_at_start(self):
        self.assertEqual(
                (datetime(2020, 5, 5), 0, 20000),
                self.monthly_profile[0])

    def test_at_1_month(self):
        self.assertEqual(
                (datetime(2020, 6, 5), 200, 19800),
                self.monthly_profile[1])
