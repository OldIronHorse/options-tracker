from unittest import TestCase
from unittest.mock import patch, MagicMock
import optionstracker.price


class TestGetPrice(TestCase):
    @patch('optionstracker.price.session')
    def test_minimal_html(self, session):
        resp = MagicMock()
        resp.html.html = ('<!DOCTYPE html>'
                          '<html lang="en">'
                          '<head></head>'
                          '<body>'
                          '<span class="bid-offer-value">'
                          '<span>48.00</span>'
                          '<span>50.00</span>'
                          '</span>'
                          '</body>'
                          '</html>')
        session.get.return_value = resp
        self.assertEqual(
                ('48.00', '50.00'),
                optionstracker.price.get_price('https://some.where.com/stock'))
        session.get.assert_called_once_with('https://some.where.com/stock')
        resp.html.render.assert_called_once()
