from unittest import TestCase

from optionstracker.tax import capital_gains_tax


# ALL CURRENCY VALUES IN PENCE
class TestCapitalGainsTax(TestCase):
    def test_no_tax_due_loss(self):
        self.assertEqual(0, capital_gains_tax(-40000))

    def test_no_tax_due_below_allowance(self):
        self.assertEqual(0, capital_gains_tax(1000000))

    def test_tax_due_at_20pc(self):
        self.assertEqual(20000, capital_gains_tax(1330000))
