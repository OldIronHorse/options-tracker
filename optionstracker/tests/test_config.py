from unittest import TestCase
from unittest.mock import patch, mock_open
from datetime import date
from io import StringIO
from optionstracker.config import grant_from_dict, grants_from_file, \
        value_grant
from optionstracker.config import Grant, Vesting, Transaction, Value


class TestGrantFromDict(TestCase):
    def test_empty_dict(self):
        with self.assertRaises(KeyError):
            grant_from_dict(dict())

    def test_valid_dict_no_txn(self):
        self.assertEqual(
                Grant(1234, 56, date(2018, 3, 15), Vesting('y', 0.25), []),
                grant_from_dict({
                    'quantity': 1234,
                    'price': 56,
                    'grant-date': date(2018, 3, 15),
                    'vesting': dict(interval='y', rate=0.25)}))

    def test_valid_dict_txns(self):
        self.assertEqual(
                Grant(1234, 56, date(2018, 3, 15), Vesting('y', 0.25), [
                    Transaction(100, 50, date(2019, 5, 12)),
                    Transaction(200, 54, date(2020, 2, 11))]),
                grant_from_dict({
                    'quantity': 1234,
                    'price': 56,
                    'grant-date': date(2018, 3, 15),
                    'vesting': dict(interval='y', rate=0.25),
                    'transactions': [
                        dict(quantity=100, price=50, date=date(2019, 5, 12)),
                        dict(quantity=200, price=54, date=date(2020, 2, 11)),
                        ]}))


class TestGrantsFromFile(TestCase):
    @patch('optionstracker.config.open', new_callable=mock_open)
    def test_valid_yaml(self, file_open):
        self.maxDiff = None
        file_open.return_value = StringIO(
                'options:\n'
                '  - quantity: 1234\n'
                '    price: 56\n'
                '    grant-date: 2018-06-01\n'
                '    vesting:\n'
                '      interval: y\n'
                '      rate: 0.3333\n'
                '    transactions:\n'
                '      -  quantity: 10\n'
                '         price: 72\n'
                '         date: 2019-12-03\n'
                '  - quantity: 7890\n'
                '    price: 12.5\n'
                '    grant-date: 2018-11-09\n'
                '    vesting:\n'
                '      interval: y\n'
                '      rate: 0.3333\n'
                '  - quantity: 123456\n'
                '    price: 78\n'
                '    grant-date: 2020-05-18\n'
                '    vesting:\n'
                '      interval: m\n'
                '      rate: 0.0277')
        self.assertEqual(
                [Grant(1234, 56, date(2018, 6, 1), Vesting('y', 0.3333), [
                    Transaction(10, 72, date(2019, 12, 3))]),
                 Grant(
                     7890, 12.5, date(2018, 11, 9), Vesting('y', 0.3333), []),
                 Grant(
                     123456, 78, date(2020, 5, 18), Vesting('m', 0.0277), [])],
                grants_from_file('/some/file/path'))
        file_open.assert_called_once_with('/some/file/path')


class TestValueGrant(TestCase):
    def test_out_of_the_money(self):
        self.assertEqual(
                Value(-13147.5, 0, -13147.5, -6577.5),
                value_grant(
                    Grant(7890, 12.5, date(2018, 11, 9),
                          Vesting('y', 0.3333), []),
                    10,
                    date(2020, 12, 2)))

    def test_in_the_money(self):
        self.assertEqual(
                Value(13147.5, 0, 13147.5, 6577.5),
                value_grant(
                    Grant(7890, 12.5, date(2018, 11, 9),
                          Vesting('y', 0.3333), []),
                    15,
                    date(2020, 12, 2)))

    def test_in_the_money_with_exercise(self):
        self.assertEqual(
                Value(13147.5, 1200, 11147.5, 6577.5),
                value_grant(
                    Grant(7890, 12.5, date(2018, 11, 9),
                          Vesting('y', 0.3333),
                          [Transaction(800, 14, date(2020, 6, 1))]),
                    15,
                    date(2020, 12, 2)))
