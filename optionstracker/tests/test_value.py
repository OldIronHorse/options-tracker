from unittest import TestCase
from datetime import datetime

from optionstracker.value import value, value_at


class TestValue(TestCase):
    def test_profit(self):
        self.assertEqual(300, value(100, 7, 10))

    def test_loss(self):
        self.assertEqual(-80, value(10, 11, 3))


class TestValueAt(TestCase):
    def test_in_first_month(self):
        self.assertEqual(
                (0, 15000),
                value_at(
                    1000, datetime(2020, 5, 5), 5, 0.01,
                    datetime(2020, 5, 25), 20))

    def test_in_second_month(self):
        self.assertEqual(
                (200, 19800),
                value_at(
                    1000, datetime(2020, 5, 5), 5, 0.01,
                    datetime(2020, 6, 25), 25))
